// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAQCZ1BUphv01BV-pfxNi29GEpoPJVwRww",
    authDomain: "projetotee.firebaseapp.com",
    databaseURL: "https://projetotee.firebaseio.com",
    projectId: "projetotee",
    storageBucket: "",
    messagingSenderId: "48048599784",
    appId: "1:48048599784:web:bb12f51355f5e4544dcf5d",
    measurementId: "G-13SZ8T1DV6"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
