//Interface que representa os passo da execução de uma tarefa
export interface Step {
    title: string;
    checked: boolean;
    
}