import { Injectable } from '@angular/core';
import { Task } from '../interfaces/task';

@Injectable({
  providedIn: 'root'
})
export class FakeDataService {

  public get(){

    const aux = localStorage.getItem('data');
    if( aux) {
      return JSON.parse(aux);
    }
  }

  set(tarefa: Task[]) {
    const aux = JSON.stringify(tarefa);
    localStorage.setItem('data', aux);
  }

}
