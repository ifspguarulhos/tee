import { Injectable } from '@angular/core';
import { FakeDataService } from './fake-data.service';
import { Step } from '../interfaces/step';

@Injectable({
  providedIn: 'root'
})
export class StepService {
  stepList: Step[];

  constructor(

    private data: FakeDataService

  ) {}

  // arrow function
  getStepList(id){
    const v = this.data.get();
    const s = v.filter(task => task.id === id);
    return s[0].steps;
  }

}
