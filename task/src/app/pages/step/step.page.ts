import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Step } from 'src/app/interfaces/step';
import { StepService } from 'src/app/services/step.service';

@Component({
  selector: 'app-step',
  templateUrl: './step.page.html',
  styleUrls: ['./step.page.scss'],
})
export class StepPage {

  lista: Step[];

  constructor(
    
    private route: ActivatedRoute,
    private service: StepService

  ) { }

  //IONIC LIFE CICLE
  ionViewWillEnter(){
    // Descubra o id da tarefa
    const id = this.route.snapshot.paramMap.get('id');
    // carregue a lista de passo da tarefa
    this.lista = this.service.getStepList(parseInt(id, 10));
    // atribua a lista de passos ao template
  }

  markStep(step) {
    // deletação
    step.checked = !step.checked;
    
  }

}
