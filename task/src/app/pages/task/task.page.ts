import { Component, OnInit } from '@angular/core';
import { TaskService } from 'src/app/services/task.service';
import { Task } from 'src/app/interfaces/task';
import { Alert } from 'selenium-webdriver';
import { AlertController } from '@ionic/angular';
import { type } from 'os';
import { format } from 'util';

@Component({
  selector: 'app-task',
  templateUrl: './task.page.html',
  styleUrls: ['./task.page.scss'],
})
export class TaskPage implements OnInit {

  //arquivos .page guardam a logica de apresentação

  lista: Task[];

  constructor(
    private service: TaskService,
    private alert: AlertController
  ) { }

  ngOnInit() {
    this.lista = this.service.taskList;
  }

  async newTask () {
    const form = await this.alert.create( {
      header: "TEE - 2019/02",
      message: "Digite o nome da tarefa",
      inputs: [{type: 'text', name: 'task'}],
      buttons: [
        {text: 'cancelar'},
        {text: 'Salvar', handler: data => this.save(data)}
        
      ]
      });

      form.present();

  }
  private save(data) {
    const idt = this.lista.length +1;
    const task: Task = {id:idt,title:data.task,steps:[]};
    this.lista.push(task);
    this.service.save(this.lista);
  }

  async renameTask(tarefa: Task) {
    const form = await this.alert.create({
      header: 'TEE - 2019/02',
      message: 'Altere o Nome da Tarefa',
      inputs: [{type: 'text', name: 'task', value: tarefa.title}],
      buttons: [
        {text: 'Cancelar'},
        {text: 'Editar', handler: data => this.edita(tarefa, data)}
      ]
    });
    form.present();
  }

  private edita(tarefa: Task, data){
    tarefa.title = data.task;
    this.service.save(this.lista);
  }

  removeTask(tarefa: Task) {
    const index = this.lista.indexOf(tarefa);
    this.lista.splice(index, 1);
    this.service.save(this.lista);
  }


}
